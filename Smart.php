<?php
	/*
		untuk memuat nilai ketika objek Smart, 
		format arraynya seperti ini : $value[criteria]['score']

		contoh : 
		- $value['fA']['score']
		- $value['B']['score']
		- $value['fC']['score']

		@function renderGraphSmartBigFive : tabel dan grafik Big Five
		@function renderGraphSmartProfile : tabel dan grafik Smart Personality Profile
		@function renderSmartInterpersonal : tabel Interpersonal
		@function renderSmartThinkingStyle : tabel Thinking Style
		@function renderSmartCopingStyle : tabel Coping style
	*/
	class Smart
	{
		public $name = '';

		public $value = array();
		
		public $path_graphline = array();

		public $criteria = array('fA', 'B', 'fC', 'fE', 'fF', 'fG', 'fH', 'fI', 'fL', 'fM', 'fN', 'fO', 'fQ1', 'fQ2', 'fQ3', 'fQ4', 'SD', 'CT');

		public $bigFiveCriteria = array(
			'E' => array(
				'code'=>'E',  
				'nameleft'=>"Introversion", 	
				'descleft'=>"Tends to feel uncomfortable in social situations.", 				
				'descright'=>"Strong predisposition to social interaction.",								
				'nameright'=>"Extraversion", 	
				'score' => 0
			),
			'N' => array(
				'code' => 'N',  
				'nameleft'=>"Low aNxiety", 		
				'descleft'=>"Calm, Composed and satisfied with life and ability to cope.", 		
				'descright'=>"Problems in coping with day to day situations. Concerned about the future.",	
				'nameright'=>"High aNxiety", 
				'score' => 0
			),
			'O' => array(
				'code' => 'O',  
				'nameleft'=>"Pragmaticism", 	
				'descleft'=>"Realistic, practical and conservative in attitudes.", 				
				'descright'=>"Enjoy innovation, interested in artistic expression.",						
				'nameright'=>"Openness", 
				'score' => 0
			),
			'A' => array(
				'code' => 'A',  
				'nameleft'=>"Independence", 	
				'descleft'=>"Alert, Quick to respond to situations, challenging, selfassured.", 
				'descright'=>"People orientated, empathic, deliberating, cautious, accommodating.",			
				'nameright'=>"Agreeableness", 	
				'score' => 0
			),
			'C' => array(
				'code' => 'C',  
				'nameleft'=>"Low Self-Control", 
				'descleft'=>"Free from constraints of social rules.",							
				'descright'=>"Conscious of group standards of behaviour.", 									
				'nameright'=>"High Self-Control",	
				'score' => 0
			),
		);

		public $ProfileCriteria = array(
			'fA' => array(
				'code'=>'A',  
				'nameleft' => "Distant Aloof", 		
				'descleft' => "Lacking empathy, Distant, Detached, Impersonal.", 			
				'descright' => "Friendly, Personable, Participating, Warm-hearted.",		
				'nameright' => "Empathic", 			
				'score' => 0
			),	 
			'B' => array(
				'code' => 'B',  
				'nameleft' => "Low Intellectance", 	
				'descleft' => "Lacking confidence in own intellectual abilities.", 		
				'descright' => "Confident of own intellectual abilities.",				
				'nameright' => "High Intellectance", 	
				'score' => 0
			),
			'fC' => array(
				'code' => 'C',  
				'nameleft' => "Affected by Feelings", 
				'descleft' => "Emotional, Changeable, Labile, Mood.", 					
				'descright' => "Mature, Calm, Phlegmatic.",								
				'nameright' => "Emotionally Stable", 	
				'score' => 0
			),
			'fE' => array(
				'code' => 'E',  
				'nameleft' => "Accommodating", 		
				'descleft' => "Passive, Mild, Humble, Deferential.", 						
				'descright' => "Assertive, Competitive, Aggressive, Forceful.",			
				'nameright' => "Dominant", 			
				'score' => 0
			),
			'fF' => array(
				'code' => 'F',  
				'nameleft' => "Sober Serious", 		
				'descleft' => "Restrained, Taciturn, Cautious.",							
				'descright' => "Lively, Cheerful, Happy-go-Lucky, Carefree.", 			
				'nameright' => "Enthusiastic",		
				'score' => 0
			),
			'fG' => array(
				'code' => 'G',  
				'nameleft' => "Expedient", 			
				'descleft' => "Spontaneous, Disregarding of rules & obligations.", 		
				'descright' => "Perservering, Dutiful, Detail conscious.",				
				'nameright' => "Conscientious", 		
				'score' => 0
			),	 
			'fH' => array(
				'code' => 'H',  
				'nameleft' => "Retiring", 			
				'descleft' => "Timid, Socially anxious, hesitant in social settings.", 	
				'descright' => "Venturesome, Talkative, Socially confident.",				
				'nameright' => "Socially-bold", 		
				'score' => 0
			),
			'fI' => array(
				'code' => 'I',  
				'nameleft' => "Hard-headed", 			
				'descleft' => "Utilitarian, Unsentimental, Lacks aesthetic sensitivity.", 
				'descright' => "Sensitive, Aesthetic aware, Sentimental.",				
				'nameright' => "Tender-minded", 		
				'score' => 0
			),
			'fL' => array(
				'code' => 'L',  
				'nameleft' => "Trusting", 			
				'descleft' => "Accepting, Unsuspecting, Credulous.", 						
				'descright' => "Sceptical, Cynical, Doubting, Critical.",					
				'nameright' => "Suspicious",			
				'score' => 0
			),
			'fM' => array(
				'code' => 'M',  
				'nameleft' => "Concrete", 			
				'descleft' => "Solution-focused, Realistic, Practical, Down-to-earth.",	
				'descright' => "Imaginative, Absent-minded, Impractical.", 				
				'nameright' => "Abstract",			
				'score' => 0
			),
			'fN' => array(
				'code' => 'N',  
				'nameleft' => "Direct", 				
				'descleft' => "Genuine, Artless, Open, Forthright, Straightforward.", 	
				'descright' => "Diplomatic, Socially astute,Socially aware, Discreet.",	
				'nameright' => "Restrained", 			
				'score' => 0
			),	 
			'fO' => array(
				'code' => 'O',  
				'nameleft' => "Confident", 			
				'descleft' => "Secure, Self-assured, Unworried, Guilt- free.", 			
				'descright' => "Worrying, Insecure, Apprehensive.",						
				'nameright' => "Self-doubting", 		
				'score' => 0
			),
			'fQ1' => array(
				'code' => 'Q1', 
				'nameleft' => "Conventional", 		
				'descleft' => "Traditional, Conservative, Conforming.", 					
				'descright' => "Experimenting, Open to change, Unconventional.",			
				'nameright' => "Radical", 			
				'score' => 0
			),
			'fQ2' => array(
				'code' => 'Q2', 
				'nameleft' => "Group-orientated", 	
				'descleft' => "Sociable, Group dependent, a 'Joiner'.", 					
				'descright' => "Solitary, Self-reliant, Individualistic.",				
				'nameright' => "Self-sufficient", 	
				'score' => 0
			),
			'fQ3' => array(
				'code' => 'Q3', 
				'nameleft' => "Informal", 			
				'descleft' => "Undisciplined, Uncontrolled, Lax, Follows own urges.",		
				'descright' => "Compulsive, Fastidious, Exacting willpower.", 			
				'nameright' => "Self-disciplined",	
				'score' => 0
			),
			'fQ4' => array(
				'code' => 'Q4', 
				'nameleft' => "Composed", 			
				'descleft' => "Relaxed, Placid, Patient.", 								
				'descright' => "Impatient, Low frustration tolerance.",					
				'nameright' => "Tense-driven", 		
				'score' => 0
			)	 
		);
		
		function __construct($value = array())
		{
			if(!empty($value)){
				foreach ($this->criteria as $key => $val_criteria) {
					if(isset($value[$val_criteria])){
						$this->value[$val_criteria] = $value[$val_criteria]['score'];
					}
				}

				$E	= ROUND((0.24*$this->value['fA'])+(0.44*$this->value['fF'])+(0.28*$this->value['fH'])-(0.34*$this->value['fQ2'])+2);
				$N	= ROUND((-0.35*$this->value['fC'])+(0.15*$this->value['fL'])+(0.31*$this->value['fO'])+(0.41*$this->value['fQ4'])+2.64);
				$O	= ROUND((0.18*$this->value['fA'])+(0.51*$this->value['fI'])+(0.38*$this->value['fM'])+(0.28*$this->value['fQ1'])-1.95);
				$A	= ROUND((-0.3*$this->value['B'])-(0.54*$this->value['fE'])-(0.33*$this->value['fL'])-(0.35*$this->value['fQ1'])+13.85);
				$C	= ROUND((0.56*$this->value['fG'])+(0.3*$this->value['fN'])+(0.45*$this->value['fQ3'])-1.65);

				if($E<1){
					$E = 1;
				}elseif($E>10){
					$E = 10;
				}

				if($N<1){
					$N = 1;
				}elseif($N>10){
					$N = 10;
				}

				if($O<1){
					$O = 1;
				}elseif($O>10){
					$O = 10;
				}

				if($A<1){
					$A = 1;
				}elseif($A>10){
					$A = 10;
				}

				if($C<1){
					$C = 1;
				}elseif($C>10){
					$C = 10;
				}

				$this->value['E'] = $E;
				$this->value['N'] = $N;
				$this->value['O'] = $O;
				$this->value['A'] = $A;
				$this->value['C'] = $C;

				/* default BIG FIVE */
				foreach ($this->bigFiveCriteria as $key => $value) {
					$ref =& $this->bigFiveCriteria[$key];

					$ref['score'] = $this->value[$key];
				}

				/* default BIG FIVE */
				foreach ($this->ProfileCriteria as $key => $value) {
					$ref =& $this->ProfileCriteria[$key];

					$ref['score'] = $this->value[$key];
				}
			}
		}

		public function renderGraphSmartBigFive($section)
		{
			$exrand = rand(100000000,999999999);

			$this->path_graphline[] = $path_graphline = 'resources/graphline/graphline_'.$exrand.'.png';

			/* BEGIN - create graph line */
			$nilai = array(1=>$this->value['E'],2=>$this->value['N'],3=>$this->value['O'],4=>$this->value['A'],5=>$this->value['C']);

			$image = imagecreatetruecolor(2000, 3500);
			imagesetthickness ( $image, 50 );
			
			// Transparent Background
			imagealphablending($image, false);
			$transparency = imagecolorallocatealpha($image, 0, 0, 0, 127);
			imagefill($image, 0, 0, $transparency);
			imagesavealpha($image, true);

			// Drawing over
			$black = imagecolorallocate($image, 0, 0, 0);
			$red = imagecolorallocate($image, 255, 0, 0);

			$barisx1 = array(1=>120,2=>320,3=>520,4=>720,5=>920,6=>1100,7=>1300,8=>1520,9=>1720,10=>1920);

			$barisy1 = array(1=>450,2=>970,3=>1500,4=>2080,5=>2600);
			$barisy2 = array(1=>750,2=>1320,3=>1830,4=>2390,5=>2830);

			$i = 1;
			while($i <= 5){
				
				$nilaix = $nilai[$i];
				$n1x1 	= $barisx1[$nilaix]; 
				$n1y1 	= $barisy1[$i];
				$n1y2 	= $barisy2[$i];

				imageline($image, $n1x1, $n1y1, $n1x1, $n1y2, $red);
				
				if($i<5){
					$j 		= $i+1;
					$nilaix = $nilai[$j];
					$n2x2 	= $barisx1[$nilaix]; 
					$n2y1 	= $barisy1[$j];
					$n2y2 	= $barisy2[$j];
					imageline($image, $n1x1, $n1y2, $n2x2, $n2y1, $red);
				}

				$i++;
			}

			imagepng($image, $path_graphline);
			/* END - create graph line */

			$color 			= array('color' => 'FFFFFF','align'=>'right','size'=>8); 
			$color1 		= array('color' => 'FFFFFF','align'=>'center','size'=>12,'bold'=>true);
			$cellBlack  	= array('valign' => 'center','align' => 'center', 'bgColor' => '#000000');
			$cellBlue  		= array('valign' => 'center','align' => 'center', 'bgColor' => '#6693ee');
			$cellBlue0 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#4585cf');
			$cellBlue1		= array('valign' => 'center','align' => 'center', 'bgColor' => '#8db3e4');
			$cellBlue2 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#c8d8f1');
			$cellPutih 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#FFFFFF');
			$textSmall 		= array('align'=>'left','size'=>7,'spaceAfter' => 0);
			$textSmallB 	= array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0);
			$tableStyle2 	= array('borderSize'=>1,'borderColor'=>'6693ee','align'=>'center');
			$cellColSpanX 	= array('gridSpan' => 14, 'align' => 'center','bgColor' => '#6693ee');

			$table = $section->addTable($tableStyle2);

			$table->addRow();

			$cell1 = $table->addCell(8500, $cellColSpanX)->addText('BIG FIVE',$color1,array('align'=>'center', 'spaceAfter'=>0 , 'spaceBefore'=>50));

			$table->addRow();

			$cell1 = $table->addCell(400, $cellBlue)->addText(htmlspecialchars('Scale'),$color,'cellGrid'); 
			$cell1 = $table->addCell(2500, $cellBlue)->addText(htmlspecialchars('Left Score'),$color,'cellGrid'); 
			$cell1 = $table->addCell(1, $cellBlue)->addImage($path_graphline, array(  'positioning'   => 'relative','wrappingStyle' => 'infront','width' => 228*0.75, 'height' => 260*0.75,'align' => 'left'));

			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('1'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('2'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('3'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('4'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('5'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('6'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('7'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('8'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('9'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('10'),$color,'cellGrid');

			$cell1 = $table->addCell(2600, $cellBlue)->addText(htmlspecialchars('Right Score'),$color,'cellGrid'); 

			$bigFive = $this->bigFiveCriteria;
			
			foreach($bigFive AS $key => $val){

				$table->addRow(300);

				$cell1 = $table->addCell(400, $cellBlue)->addText(htmlspecialchars($val['code']),$color,'cellGrid'); 
				$cell1 = $table->addCell(2500);
				$cell1->addText(htmlspecialchars($val['nameleft']),$textSmallB,'iStyle');
				$cell1->addText(htmlspecialchars($val['descleft']),$textSmall,'dStyle');  
				$cell1 = $table->addCell(1, $cellBlue);

				$i=1; 

				while($i<=10){

					if($i==1 || $i==2 || $i==9 || $i==10){
						$cell = $cellBlue0;
					}elseif($i==3 || $i==8){
						$cell = $cellBlue1;
					}elseif($i==4 || $i==7){
						$cell = $cellBlue2;
					}elseif($i==5 || $i==6){
						$cell = $cellPutih;
					}

					if($i==1){
						$point = 'resources/bluedot_ki.png';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT;
						$lbr   = 35;
					}elseif($i==10){
						$point = 'resources/bluedot_ka.png';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT;
						$lbr   = 35;
					}else{
						$point = 'resources/bluedot.png';
						$lbr   = 45;
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER;
					}

					if($i==$val['score']){
				    	$cell2 = $table->addCell(340, $cell)->addImage($point, array(
							'positioning'   => 'absolute',
							'wrappingStyle' => 'infront',
							'width' => $lbr*0.75, 
							'height' => 20*0.75,
							'posHorizontal'    => $pos,
				        	'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
				        	'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_CENTER,
				        	'posVerticalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_LINE,
				    	));
				    }else{
					    $cell3 = $table->addCell(340, $cell)->addText(htmlspecialchars(' '),null,'cellGrid');
					}

					$i++;
				}

				$cell1 = $table->addCell(2600);
				$cell1->addText(htmlspecialchars($val['nameright']),$textSmallB,'iStyle');
				$cell1->addText(htmlspecialchars($val['descright']),$textSmall,'dStyle');  
			}
		}

		public function renderGraphSmartProfile($section)
		{
			$exrand = rand(100000000,999999999);

			$this->path_graphline[] = $path_graphline = 'resources/graphline/graphline1_'.$exrand.'.png';

			/* BEGIN - create graph line */
			ini_set('memory_limit', 1024 * 1024 * 1024); 

			$nilai = array(
				1 => $this->value['fA'],
				2 => $this->value['B'],
				3 => $this->value['fC'],
				4 => $this->value['fE'],
				5 => $this->value['fF'],
				6 => $this->value['fG'],
				7 => $this->value['fH'],
				8 => $this->value['fI'],
				9 => $this->value['fL'],
				10 => $this->value['fM'],
				11 => $this->value['fN'],
				12 => $this->value['fO'],
				13 => $this->value['fQ1'],
				14 => $this->value['fQ2'],
				15 => $this->value['fQ3'],
				16 => $this->value['fQ4']
			);

			$image = imagecreatetruecolor(1000, 4300);
			imagesetthickness ( $image, 25 );

			// Transparent Background
			imagealphablending($image, false);
			$transparency = imagecolorallocatealpha($image, 0, 0, 0, 127);
			imagefill($image, 0, 0, $transparency);
			imagesavealpha($image, true);

			// Drawing over
			$black = imagecolorallocate($image, 0, 0, 0);
			$red = imagecolorallocate($image, 255, 0, 0);
			 
			$barisx1 = array(1=>55,2=>155,3=>260,4=>355,5=>450,6=>555,7=>650,8=>750,9=>860,10=>955);
			$pem = 0;
			$barisy1 = array(1=>225-$pem,2=>505-$pem,3=>740-$pem,4=>980-$pem,5=>1255-$pem,6=>1530-$pem,7=>1790-$pem,8=>2075-$pem,9=>2315-$pem,10=>2550-$pem,11=>2825-$pem,12=>3100-$pem,13=>3385-$pem,14=>3615-$pem,15=>3860-$pem,16=>4100-$pem);
			$barisy2 = array(1=>375-$pem,2=>650-$pem,3=>890-$pem,4=>1130-$pem,5=>1405-$pem,6=>1680-$pem,7=>1950-$pem,8=>2230-$pem,9=>2460-$pem,10=>2700-$pem,11=>2975-$pem,12=>3260-$pem,13=>3525-$pem,14=>3765-$pem,15=>3995-$pem,16=>4230-$pem);

			$i = 1;
			while($i <= 16){
				
				$nilaix = $nilai[$i];
				$n1x1 	= $barisx1[$nilaix]; 
				$n1y1 	= $barisy1[$i];
				$n1y2 	= $barisy2[$i];

				imageline($image, $n1x1, $n1y1, $n1x1, $n1y2, $red);
				
				if($i<16){
					$j 		= $i+1;
					$nilaix = $nilai[$j];
					$n2x2 	= $barisx1[$nilaix]; 
					$n2y1 	= $barisy1[$j];
					$n2y2 	= $barisy2[$j];
					imageline($image, $n1x1, $n1y2, $n2x2, $n2y1, $red);
				}	
				$i++;
			}

			imagepng($image, $path_graphline);
			/* END - create graph line */

			$color 			= array('color' => 'FFFFFF','align'=>'right','size'=>8); 
			$color1 		= array('color' => 'FFFFFF','align'=>'center','size'=>12,'bold'=>true);
			$cellBlack  	= array('valign' => 'center','align' => 'center', 'bgColor' => '#000000');
			$cellBlue  		= array('valign' => 'center','align' => 'center', 'bgColor' => '#6693ee');
			$cellBlue0 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#4585cf');
			$cellBlue1		= array('valign' => 'center','align' => 'center', 'bgColor' => '#8db3e4');
			$cellBlue2 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#c8d8f1');
			$cellPutih 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#FFFFFF');
			$textSmall 		= array('align'=>'left','size'=>7,'spaceAfter' => 0);
			$textSmallB 	= array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0);
			$tableStyle2 	= array('borderSize'=>1,'borderColor'=>'6693ee','align'=>'center');
			$cellColSpanX 	= array('gridSpan' => 14, 'align' => 'center','bgColor' => '#6693ee');
			
			$table = $section->addTable($tableStyle2);

			$table->addRow();
			$cell1 = $table->addCell(8500, $cellColSpanX)->addText('SMART Personality Profile',$color1,array('align'=>'center', 'spaceAfter'=>0 , 'spaceBefore'=>50));

			$table->addRow();

			$cell1 = $table->addCell(400, $cellBlue)->addText(htmlspecialchars('Scale'),$color,'cellGrid'); 
			$cell1 = $table->addCell(2500, $cellBlue)->addText(htmlspecialchars('Left Score'),$color,'cellGrid'); 
			$cell1 = $table->addCell(1, $cellBlue)->addImage($path_graphline, array(  'positioning'   => 'relative','wrappingStyle' => 'infront','width' => 228*0.75, 'height' => 630*0.75,'align' => 'left'));

			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('1'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('2'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('3'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('4'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('5'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('6'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('7'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('8'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('9'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('10'),$color,'cellGrid');

			$cell1 = $table->addCell(2600, $cellBlue)->addText(htmlspecialchars('Right Score'),$color,'cellGrid'); 

			$beast = $this->ProfileCriteria;

			foreach($beast AS $key => $val){

				$table->addRow(300);

				$cell1 = $table->addCell(400, $cellBlue)->addText(htmlspecialchars($val['code']),$color,'cellGrid'); 
				$cell1 = $table->addCell(2500);
				$cell1->addText(htmlspecialchars($val['nameleft']),$textSmallB,'iStyle');
				$cell1->addText(htmlspecialchars($val['descleft']),$textSmall,'dStyle');  

				$cell1 = $table->addCell(1, $cellBlue);

				$i=1; 

				while($i<=10){

					if($i==1 || $i==2 || $i==9 || $i==10){
						$cell = $cellBlue0;
					}elseif($i==3 || $i==8){
						$cell = $cellBlue1;
					}elseif($i==4 || $i==7){
						$cell = $cellBlue2;
					}elseif($i==5 || $i==6){
						$cell = $cellPutih;
					}

					if($i==1){
						$point = 'resources/bluedot_ki.png';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT;
						$lbr   = 35;
					}elseif($i==10){
						$point = 'resources/bluedot_ka.png';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT;
						$lbr   = 35;
					}else{
						$point = 'resources/bluedot.png';
						$lbr   = 45;
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER;
					}

					if($i==$val['score']){
				    	$cell2 = $table->addCell(340, $cell)->addImage($point, array(
							'positioning'   => 'relative',
							'wrappingStyle' => 'infront',
							'width' => $lbr*0.75, 
							'height' => 20*0.75,
							'posHorizontal'    => $pos,
							'marginTop'=> -0.2,
				        	'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
						
				    	));
				    }else{
					    $cell3 = $table->addCell(340, $cell)->addText(htmlspecialchars(' '),null,'cellGrid');
					}

					$i++;
				}

				$cell1 = $table->addCell(2600);
				$cell1->addText(htmlspecialchars($val['nameright']),$textSmallB,'iStyle');
				$cell1->addText(htmlspecialchars($val['descright']),$textSmall,'dStyle');  
		 
			}
		}

		public function renderSmartInterpersonal($section)
		{
			$E 		= $this->value['E'];
			$fA 	= $this->value['fA'];
			$fF 	= $this->value['fF'];
			$fH 	= $this->value['fH'];
			$fQ2 	= $this->value['fQ2'];
			$A 		= $this->value['A'];
			$B 		= $this->value['B'];
			$fE 	= $this->value['fE'];
			$fL 	= $this->value['fL'];
			$fQ1 	= $this->value['fQ1'];

			$color 			= array('color' => 'FFFFFF','align'=>'right','size'=>8); 
			$color1 		= array('color' => 'FFFFFF','align'=>'center','size'=>12,'bold'=>true);
			$cellBlack  	= array('valign' => 'center','align' => 'center', 'bgColor' => '#000000');
			$cellBlue  		= array('valign' => 'center','align' => 'center', 'bgColor' => '#6693ee');
			$cellBlue0 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#4585cf');
			$cellBlue1		= array('valign' => 'center','align' => 'center', 'bgColor' => '#8db3e4');
			$cellBlue2 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#c8d8f1');
			$cellPutih 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#FFFFFF');
			$textSmall 		= array('align'=>'left','size'=>7,'spaceAfter' => 0);
			$textSmallB 	= array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0);
			$tableStyle2 	= array('borderSize'=>1,'borderColor'=>'6693ee','align'=>'center');
			$cellColSpanX 	= array('gridSpan' => 14, 'align' => 'center','bgColor' => '#6693ee');
			$cellColSpan 	= array('gridSpan' => 13, 'align' => 'center','bgColor' => '#6693ee');

			$table = $section->addTable($tableStyle2);

			$table->addRow();
			$cell1 = $table->addCell(8500, $cellColSpan)->addText('INTERPERSONAL STYLE ',$color1,array('align'=>'center', 'spaceAfter'=>0 , 'spaceBefore'=>50));

			$table->addRow();

			$cell1 = $table->addCell(400, $cellBlue)->addText(htmlspecialchars('Scale'),$color,'cellGrid'); 
			$cell1 = $table->addCell(2500, $cellBlue)->addText(htmlspecialchars('Left Score'),$color,'cellGrid'); 

			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('1'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('2'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('3'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('4'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('5'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('6'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('7'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('8'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('9'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellBlue)->addText(htmlspecialchars('10'),$color,'cellGrid');

			$cell1 = $table->addCell(2600, $cellBlue)->addText(htmlspecialchars('Right Score'),$color,'cellGrid'); 

			$interStyle1 = array(
				array('code'=>'E', 'nameleft'=>"Introversion", 'descleft'=>"Tends to feel uncomfortable in social situations.", 'descright'=>"Strong predisposition to social interaction.",'nameright'=>"Extraversion", 'score'=>$E),	 
				array('code'=>'fA', 'nameleft'=>"Distant Aloof", 'descleft'=>"Lacking empathy, Distant, Detached, Impersonal.", 'descright'=>"Friendly, Personable, Participating, Warm-hearted.",'nameright'=>"Empathic", 'score'=>$fA),
				array('code'=>'fF', 'nameleft'=>"Sober Serious", 'descleft'=>"Restrained, Taciturn, Cautious.", 'descright'=>"Lively, Cheerful, Happy-go-Lucky, Carefree.",'nameright'=>"Enthusiastic", 'score'=>$fF),
				array('code'=>'fH', 'nameleft'=>"Retiring", 'descleft'=>"Timid, Socially anxious, hesitant in social settings.", 'descright'=>"Venturesome, Talkative, Socially confident.",'nameright'=>"Socially-bold", 'score'=>$fH),
				array('code'=>'Q2', 'nameleft'=>"Self-sufficient", 'descleft'=>"Solitary, Self-reliant, Individualistic.",'descright'=>"Sociable, Group dependent, a 'Joiner'.", 'nameright'=>"Group-orientated",'score'=>11-$fQ2)
			);

			$interStyle2 = array(
				array('code'=>'A', 'nameleft'=>"Independence", 'descleft'=>"Alert, Quick to respond to situations, challenging, selfassured.", 'descright'=>"People orientated, empathic, deliberating, cautious, accommodating.",'nameright'=>"Agreeableness", 'score'=>$A),	 
				array('code'=>'B', 'nameleft'=>"High Intellectance", 'descleft'=>"Confident of own intellectual abilities.", 'descright'=>"Lacking confidence in own intellectual abilities.",'nameright'=>"Low Intellectance", 'score'=>11-$B),
				array('code'=>'fE', 'nameleft'=>"Dominant", 'descleft'=>"Assertive, Competitive, Aggressive, Forceful.", 'descright'=>"Passive, Mild, Humble, Deferential.",'nameright'=>"Accommodating", 'score'=>11-$fE),
				array('code'=>'fL', 'nameleft'=>"Suspicious", 'descleft'=>"Sceptical, Cynical, Doubting, Critical.", 'descright'=>"Accepting, Unsuspecting, Credulous.",'nameright'=>"Trusting", 'score'=>11-$fL),
				array('code'=>'Q1', 'nameleft'=>"Radical", 'descleft'=>"Experimenting, Open to change, Unconventional.",'descright'=>"Traditional, Conservative, Conforming.", 'nameright'=>"Conventional",'score'=>11-$fQ1)
			);

			foreach($interStyle1 AS $key => $val){

				$table->addRow(300);

				if($val['code']=="E"){
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0,'color'=>'#6693ee');
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0,'color'=>'#6693ee');
				}else{
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0);
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0);

				}

				$cell1 = $table->addCell(400, $cellBlue)->addText(htmlspecialchars($val['code']),$color,'cellGrid'); 
				$cell1 = $table->addCell(2500);
				$cell1->addText($val['nameleft'],$textSmallB,'iStyle');
				$cell1->addText($val['descleft'],$textSmall,'dStyle');  
				
				$i=1; 

				while($i<=10){

					if($i==1 || $i==2 || $i==9 || $i==10){
						$cell = $cellBlue0;
					}elseif($i==3 || $i==8){
						$cell = $cellBlue1;
					}elseif($i==4 || $i==7){
						$cell = $cellBlue2;
					}elseif($i==5 || $i==6){
						$cell = $cellPutih;
					}

					if($i==1){
						$point = 'resources/blupol_ki.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT;
						$lbr   = 35;
					}elseif($i==10){
						$point = 'resources/blupol_ka.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT;
						$lbr   = 35;
					}else{
						$point = 'resources/blupol.jpg';
						$lbr   = 45;
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER;
					}

					if($i==$val['score']){
				    	$cell2 = $table->addCell(340, $cell)->addImage($point, array(
							'positioning'   => 'relative',
							'wrappingStyle' => 'infront',
							'width' => $lbr*0.75, 
							'height' => 20*0.75,
							'posHorizontal'    => $pos,
				        	'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
						
				    	));
				    }else{
					    $cell3 = $table->addCell(340, $cell)->addText(htmlspecialchars(' '),null,'cellGrid');
					}

					$i++;
				}

				$cell1 = $table->addCell(2600);
				$cell1->addText($val['nameright'],$textSmallB,'iStyle');
				$cell1->addText($val['descright'],$textSmall,'dStyle');  
			}

			// hitam
			$table->addRow(); 
			$cell1 = $table->addCell(9000, array('gridSpan'=>13,'bgColor'=>'#000000'))->addText(htmlspecialchars(' '),null,array('spaceAfter'=>0));

			foreach($interStyle2 AS $key => $val){

				$table->addRow(300);

				if($val['code']=="A"){
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0,'color'=>'#6693ee');
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0,'color'=>'#6693ee');
				}else{
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0);
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0);
				}

				$cell1 = $table->addCell(400, $cellBlue)->addText(htmlspecialchars($val['code']),$color,'cellGrid'); 
				$cell1 = $table->addCell(2500);
				$cell1->addText($val['nameleft'],$textSmallB,'iStyle');
				$cell1->addText($val['descleft'],$textSmall,'dStyle'); 
				
				$i=1; 

				while($i<=10){

					if($i==1 || $i==2 || $i==9 || $i==10){
						$cell = $cellBlue0;
					}elseif($i==3 || $i==8){
						$cell = $cellBlue1;
					}elseif($i==4 || $i==7){
						$cell = $cellBlue2;
					}elseif($i==5 || $i==6){
						$cell = $cellPutih;
					}

					if($i==1){
						$point = 'resources/blupol_ki.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT;
						$lbr   = 35;
					}elseif($i==10){
						$point = 'resources/blupol_ka.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT;
						$lbr   = 35;
					}else{
						$point = 'resources/blupol.jpg';
						$lbr   = 45;
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER;
					}

					if($i==$val['score']){
				    	$cell2 = $table->addCell(340, $cell)->addImage($point, array(
							'positioning'   => 'relative',
							'wrappingStyle' => 'infront',
							'width' => $lbr*0.75, 
							'height' => 20*0.75,
							'posHorizontal'    => $pos,
				        	'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
						
				    	));
				    }else{
					    $cell3 = $table->addCell(340, $cell)->addText(htmlspecialchars(' '),null,'cellGrid');
					}

					$i++;
				}

				$cell1 = $table->addCell(2600);
				$cell1->addText($val['nameright'],$textSmallB,'iStyle');
				$cell1->addText($val['descright'],$textSmall,'dStyle');  
			}
		}

		public function renderSmartThinkingStyle($section)
		{
			$O 		= $this->value['O'];
			$fI 	= $this->value['fI'];
			$fM 	= $this->value['fM'];
			$fQ1 	= $this->value['fQ1'];
			$C 		= $this->value['C'];
			$fG 	= $this->value['fG'];
			$fN 	= $this->value['fN'];
			$fQ3 	= $this->value['fQ3'];

			$color 			= array('color' => 'FFFFFF','align'=>'right','size'=>8);
			$color1 		= array('color' => 'FFFFFF','align'=>'center','size'=>12,'bold'=>true);
			$tableStyle2 	= array('borderSize'=>1,'borderColor'=>'d05c5f','align'=>'center');
			$cellColSpanM 	= array('gridSpan' => 13, 'align' => 'center','bgColor' => '#d05c5f');
			$cellMerah 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#d05c5f');
			$cellMerah0		= array('valign' => 'center','align' => 'center', 'bgColor' => '#f55257');
			$cellMerah1		= array('valign' => 'center','align' => 'center', 'bgColor' => '#f99192');
			$cellMerah2		= array('valign' => 'center','align' => 'center', 'bgColor' => '#fdcdcd');
			$cellPutih 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#FFFFFF');

			$thinkStyle = array(
				array('code'=>'O', 'nameleft'=>"Pragmaticism", 'descleft'=>"Realistic, practical and conservative in attitudes.", 'descright'=>"Enjoy innovation, interested in artistic expression.",'nameright'=>"Openness", 'score'=>$O),	 
				array('code'=>'fI', 'nameleft'=>"Hard-headed", 'descleft'=>"Utilitarian, Unsentimental, Lacks aesthetic sensitivity.", 'descright'=>"Sensitive, Aesthetic aware, Sentimental.",'nameright'=>"Tender-minded", 'score'=>$fI),
				array('code'=>'fM', 'nameleft'=>"Concrete", 'descleft'=>"Solution-focused, Realistic, Practical, Down-to-earth.", 'descright'=>"Imaginative, Absent-minded, Impractical.",'nameright'=>"Abstract", 'score'=>$fM),
				array('code'=>'fQ1', 'nameleft'=>"Conventional", 'descleft'=>"Traditional, Conservative, Conforming.", 'descright'=>"Experimenting, Open to change, Unconventional.",'nameright'=>"Radical", 'score'=>$fQ1)
			);

			$thinkStyle2 = array(
				array('code'=>'C', 'nameleft'=>"Low Self-Control", 'descleft'=>"Free from constraints of social rules.", 'descright'=>"Conscious of group standards of behaviour." ,'nameright'=>"High Self-Control", 'score'=>$C),	 
				array('code'=>'fG', 'nameleft'=>"Expedient", 'descleft'=>"Spontaneous, Disregarding of rules & obligations.", 'descright'=>"Perservering, Dutiful, Detail conscious." ,'nameright'=>"Conscientious", 'score'=>$fG),
				array('code'=>'fN', 'nameleft'=>"Direct", 'descleft'=>"Genuine, Artless, Open, Forthright, Straightforward.", 'descright'=>"Diplomatic, Socially astute,Socially aware, Discreet." ,'nameright'=>"Restrained", 'score'=>$fN),
				array('code'=>'fQ3', 'nameleft'=>"Informal", 'descleft'=>"Undisciplined, Uncontrolled, Lax, Follows own urges.", 'descright'=>"Compulsive, Fastidious, Exacting willpower." ,'nameright'=>"Self-disciplined", 'score'=>$fQ3)
			);
			 
			$table = $section->addTable($tableStyle2);

			$table->addRow();
			$cell1 = $table->addCell(8500, $cellColSpanM)->addText('THINKING STYLE',$color1,array('align'=>'center', 'spaceAfter'=>0));

			$table->addRow();

			$cell1 = $table->addCell(400, $cellMerah)->addText(htmlspecialchars('Scale'),$color,'cellGrid'); 
			$cell1 = $table->addCell(2500, $cellMerah)->addText(htmlspecialchars('Left Score'),$color,'cellGrid'); 

			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('1'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('2'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('3'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('4'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('5'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('6'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('7'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('8'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('9'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellMerah)->addText(htmlspecialchars('10'),$color,'cellGrid');

			$cell1 = $table->addCell(2600, $cellMerah)->addText(htmlspecialchars('Right Score'),$color,'cellGrid'); 
			
			foreach($thinkStyle AS $key => $val){

				$table->addRow(300);

				if($val['code']=="O"){
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0,'color'=>'#d05c5f');
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0,'color'=>'#d05c5f');
				}else{
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0);
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0);

				}

				$cell1 = $table->addCell(400, $cellMerah)->addText(htmlspecialchars($val['code']),$color,'cellGrid'); 
				$cell1 = $table->addCell(2500);
				$cell1->addText($val['nameleft'],$textSmallB,'iStyle');
				$cell1->addText($val['descleft'],$textSmall,'dStyle'); 
				
				$i=1; 

				while($i<=10){

					if($i==1 || $i==2 || $i==9 || $i==10){
						$cell = $cellMerah0;
					}elseif($i==3 || $i==8){
						$cell = $cellMerah1;
					}elseif($i==4 || $i==7){
						$cell = $cellMerah2;
					}elseif($i==5 || $i==6){
						$cell = $cellPutih;
					}

					if($i==1){
						$point = 'resources/redpol_ki.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT;
						$lbr   = 35;
					}elseif($i==10){
						$point = 'resources/redpol_ka.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT;
						$lbr   = 35;
					}else{
						$point = 'resources/redpol.jpg';
						$lbr   = 45;
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER;
					}

					if($i==$val['score']){
				    	$cell2 = $table->addCell(340, $cell)->addImage($point, array(
							'positioning'   => 'relative',
							'wrappingStyle' => 'infront',
							'width' => $lbr*0.75, 
							'height' => 20*0.75,
							'posHorizontal'    => $pos,
				        	'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
						
				    	));
				    }else{
					    $cell3 = $table->addCell(340, $cell)->addText(htmlspecialchars(' '),null,'cellGrid');
					}

					$i++;
				}

				$cell1 = $table->addCell(2600);
				$cell1->addText($val['nameright'],$textSmallB,'iStyle');
				$cell1->addText($val['descright'],$textSmall,'dStyle');  
		 
			}

			// hitam
			$table->addRow(); 
			$cell1 = $table->addCell(9000, array('gridSpan'=>13,'bgColor'=>'#000000'))->addText(htmlspecialchars(' '),null,array('spaceAfter'=>0));

			foreach($thinkStyle2 AS $key => $val){

				$table->addRow(300);

				if($val['code']=="C"){
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0,'color'=>'#d05c5f');
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0,'color'=>'#d05c5f');
				}else{
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0);
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0);

				}

				$cell1 = $table->addCell(400, $cellMerah)->addText(htmlspecialchars($val['code']),$color,'cellGrid'); 
				$cell1 = $table->addCell(2500);
				$cell1->addText(htmlspecialchars($val['nameleft']),$textSmallB,'iStyle');
				$cell1->addText(htmlspecialchars($val['descleft']),$textSmall,'dStyle');  
				
				$i=1; 

				while($i<=10){

					if($i==1 || $i==2 || $i==9 || $i==10){
						$cell = $cellMerah0;
					}elseif($i==3 || $i==8){
						$cell = $cellMerah1;
					}elseif($i==4 || $i==7){
						$cell = $cellMerah2;
					}elseif($i==5 || $i==6){
						$cell = $cellPutih;
					}

					if($i==1){
						$point = 'resources/redpol_ki.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT;
						$lbr   = 35;
					}elseif($i==10){
						$point = 'resources/redpol_ka.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT;
						$lbr   = 35;
					}else{
						$point = 'resources/redpol.jpg';
						$lbr   = 45;
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER;
					}

					if($i==$val['score']){
				    	$cell2 = $table->addCell(340, $cell)->addImage($point, array(
							'positioning'   => 'relative',
							'wrappingStyle' => 'infront',
							'width' => $lbr*0.75, 
							'height' => 20*0.75,
							'posHorizontal'    => $pos,
				        	'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
						
				    	));
				    }else{
					    $cell3 = $table->addCell(340, $cell)->addText(htmlspecialchars(' '),null,'cellGrid');
					}
					
					$i++;
				}

				$cell1 = $table->addCell(2600);
				$cell1->addText(htmlspecialchars($val['nameright']),$textSmallB,'iStyle');
				$cell1->addText(htmlspecialchars($val['descright']),$textSmall,'dStyle');  
			}
		}

		public function renderSmartCopingStyle($section)
		{
			$N 		= $this->value['N'];
			$fC 	= $this->value['fC'];
			$fO 	= $this->value['fO'];
			$fQ4 	= $this->value['fQ4'];
			$fL 	= $this->value['fL'];

			$color 			= array('color' => 'FFFFFF','align'=>'right','size'=>8);
			$color1 		= array('color' => 'FFFFFF','align'=>'center','size'=>12,'bold'=>true);
			$tableStyle2 	= array('borderSize'=>1,'borderColor'=>'308330','align'=>'center');
			$cellColSpanH 	= array('gridSpan' => 13, 'align' => 'center','bgColor' => '#308330');
			$cellHijau 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#308330');
			$cellHijau0		= array('valign' => 'center','align' => 'center', 'bgColor' => '#30AC30');
			$cellHijau1		= array('valign' => 'center','align' => 'center', 'bgColor' => '#45B445');
			$cellHijau2		= array('valign' => 'center','align' => 'center', 'bgColor' => '#7DCA7D');
			$cellPutih 		= array('valign' => 'center','align' => 'center', 'bgColor' => '#FFFFFF');

			$copingStyle = array(
				array('code'=>'N', 'nameleft'=>"Low aNxiety", 'descleft'=>"Calm, Composed and satisfied with life and ability to cope.", 'descright'=>"Problems in coping with day to day situations. Concerned about the future.",'nameright'=>"High aNxiety", 'score'=>$N),	 
				array('code'=>'fC', 'nameleft'=>"Emotionally Stable", 'descleft'=>"Mature, Calm, Phlegmatic.", 'descright'=>"Emotional, Changeable, Labile, Mood.",'nameright'=>"Affected by Feelings", 'score'=>11-$fC),
				array('code'=>'fO', 'nameleft'=>"Confident", 'descleft'=>"Secure, Self-assured, Unworried, Guilt- free.", 'descright'=>"Worrying, Insecure, Apprehensive.",'nameright'=>"Self-doubting", 'score'=>$fO),
				array('code'=>'fQ4', 'nameleft'=>"Composed", 'descleft'=>"Relaxed, Placid, Patient.", 'descright'=>"Impatient, Low frustration tolerance.",'nameright'=>"Tense-driven", 'score'=>$fQ4),
				array('code'=>'fL', 'nameleft'=>"Trusting", 'descleft'=>"Accepting, Unsuspecting, Credulous.", 'descright'=>"Sceptical, Cynical, Doubting, Critical.",'nameright'=>"Suspicious", 'score'=>$fL)
			);

			$table = $section->addTable($tableStyle2);

			$table->addRow();
			$cell1 = $table->addCell(8500, $cellColSpanH)->addText('COPING STYLE ',$color1,array('align'=>'center', 'spaceAfter'=>0));

			$table->addRow();

			$cell1 = $table->addCell(400, $cellHijau)->addText(htmlspecialchars('Scale'),$color,'cellGrid'); 
			$cell1 = $table->addCell(2500, $cellHijau)->addText(htmlspecialchars('Left Score'),$color,'cellGrid'); 

			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('1'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('2'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('3'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('4'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('5'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('6'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('7'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('8'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('9'),$color,'cellGrid');
			$cell1 = $table->addCell(320, $cellHijau)->addText(htmlspecialchars('10'),$color,'cellGrid');

			$cell1 = $table->addCell(2600, $cellHijau)->addText(htmlspecialchars('Right Score'),$color,'cellGrid'); 

			foreach($copingStyle AS $key => $val){

				$table->addRow(300);

				if($val['code']=="N"){
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0,'color'=>'#308330');
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0,'color'=>'#308330');
				}else{
					$textSmall 	= array('align'=>'left','size'=>7,'spaceAfter' => 0);
					$textSmallB = array('align'=>'left', 'size'=>8, 'bold'=>true, 'spaceAfter' => 0);

				}

				$cell1 = $table->addCell(400, $cellHijau)->addText(htmlspecialchars($val['code']),$color,'cellGrid'); 
				$cell1 = $table->addCell(2500);
				$cell1->addText($val['nameleft'],$textSmallB,'iStyle');
				$cell1->addText($val['descleft'],$textSmall,'dStyle');  
				
				$i=1; 

				while($i<=10){

					if($i==1 || $i==2 || $i==9 || $i==10){
						$cell = $cellHijau0;
					}elseif($i==3 || $i==8){
						$cell = $cellHijau1;
					}elseif($i==4 || $i==7){
						$cell = $cellHijau2;
					}elseif($i==5 || $i==6){
						$cell = $cellPutih;
					}

					if($i==1){
						$point = 'resources/ijopol_ki.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT;
						$lbr   = 35;
					}elseif($i==10){
						$point = 'resources/ijopol_ka.jpg';
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT;
						$lbr   = 35;
					}else{
						$point = 'resources/ijopol.jpg';
						$lbr   = 45;
						$pos  = \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER;
					}

					if($i==$val['score']){
				    	$cell2 = $table->addCell(340, $cell)->addImage($point, array(
							'positioning'   => 'relative',
							'wrappingStyle' => 'infront',
							'width' => $lbr*0.75, 
							'height' => 20*0.75,
							'posHorizontal'    => $pos,
				        	'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
						
				    	));
				    }else{
					    $cell3 = $table->addCell(340, $cell)->addText(htmlspecialchars(' '),null,'cellGrid');
					}

					$i++;
				}

				$cell1 = $table->addCell(2600);
				$cell1->addText($val['nameright'],$textSmallB,'iStyle');
				$cell1->addText($val['descright'],$textSmall,'dStyle');  
			}
		}
	}
?>