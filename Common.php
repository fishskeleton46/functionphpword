<?php
	class Common 
	{
		public $warnaPurpose = '#2b76c1';
		public $namaCandidate = '';
		public $purpose = '';
		public $array = array();
		public $section;
		public $CandidateAttribute = array();
		public $idcnd;

		public function __construct($value=''){
			$idcnd = isset($_GET['idcnd']) ? $_GET['idcnd'] : '';

			$this->idcnd = $idcnd;
		}

		function init($phpWord){
			$phpWord->addNumberingStyle(
			    'multilevel',
			    array(
			        'type'   => 'multilevel',
			        'levels' => array(
			            array('format' => 'bullet', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
			        ),
			    )
			); 

			$phpWord->addParagraphStyle('gStyle', array('align' => 'right','spaceAfter' =>0,'spaceBefore'=>70));
			$phpWord->addParagraphStyle('leftStyle', array('align' => 'left','spaceAfter' =>0,'spaceBefore'=>70));
			$phpWord->addParagraphStyle('listGrid', array('spaceBefore' =>0 ,'spaceAfter' =>0 ,'align' => 'left'));
			$phpWord->addParagraphStyle('isiGrid', array('spaceBefore' =>50 ,'spaceAfter' =>50 ,'align' => 'left'));
			$phpWord->addParagraphStyle('centerGrid', array('spaceBefore' =>0 ,'spaceAfter' =>0 ,'align' => 'center','valign' => 'center'));
			$phpWord->addParagraphStyle('cellGrid', array('spaceBefore' =>80 ,'spaceAfter' =>50 ,'align' => 'center'));
			$phpWord->addParagraphStyle('cellGridBig', array('spaceBefore' =>0 ,'spaceAfter' =>0 ,'align' => 'center'));
			$phpWord->addParagraphStyle('cellSmall', array('spaceBefore' =>00 ,'spaceAfter' =>00 ,'align' => 'center'));
			$phpWord->addParagraphStyle('pStyle', array('align' => 'both','spaceBefore' =>10 ,'spaceAfter' =>110 ));
			$phpWord->addParagraphStyle('rStyle', array('align' => 'both','spaceBefore' =>100 ,'spaceAfter' =>0 ));
			$phpWord->addParagraphStyle('dStyle', array('align' => 'both','spaceBefore' =>130 ,'spaceAfter' =>110 ));
			$phpWord->addParagraphStyle('qStyle', array('align' => 'center' )); 
			$phpWord->addParagraphStyle('cStyle', array('align' => 'center', 'spaceBefore'=>90,'spaceAfter'=>40));
			$phpWord->addParagraphStyle('header1', array('align'=>'center','spaceAfter'=>0,'spaceBefore'=>50));
		  	$phpWord->addParagraphStyle('bothGrid', array('spaceBefore' =>0 ,'spaceAfter' =>0 ,'align' => 'both'));

		  	$styleTable = array('borderSize' => 6, 'borderColor' => '#000', 'cellMargin' => 120,'align'=>'center');
			
			$phpWord->addTableStyle('Colspan Rowspan', $styleTable);

		  	return $phpWord;
		}
		
		function addSection($phpWord){
			$sectio = $phpWord->addSection(array(
				'headerHeight'=> \PhpOffice\PhpWord\Shared\Converter::inchToTwip(.2),
				'marginLeft'=>600,
				'marginRight'=>600)
			);

			return $sectio;
		}

		function tableHeader($section, $company_logo = null, $with_line = false){			
			$sec2header = $section->addHeader();

			$table    = $sec2header->addTable(array(
				'cellMarginTop'=>0,
				'cellMarginBottom'=>0,
				'cellMarginLeft'=>40,
				'cellMarginRight'=>40)
			);

			$table->addRow();
			
			$table->addCell(4000)->addImage(
				'resources/smart-logo.png',
				array('height' => 55*0.75, 'align' => 'left') 
			);

			if(!empty($company_logo)){
				$table->addCell(7000)->addImage(
					'resources/'.$company_logo,
					array(
						'positioning'   => 'relative',
						'wrappingStyle' => 'infront',
						'align'=>'right', 
						'height' => 70*0.65,
						'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
						'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN
					)
				);
			}

			if($with_line){
				$namingWay = array('color'=>'#FFFFFF','size'=>10, 'bold'=>true);

				$table->addRow();
				
				$text = 'report';
				if(!empty($this->purpose)){
					$text = $this->purpose.' '.$text;
				}

				$text = strtoupper($text);

				$table->addCell(4500, array('bgColor'=>$this->warnaPurpose,'align'=>'left'))->addText($text, $namingWay,'leftStyle');
				$table->addCell(6000, array('bgColor'=>$this->warnaPurpose,'align'=>'right'))->addText(strtoupper($this->namaCandidate),  $namingWay,'gStyle');

				$footer = $section->addFooter();
				$footer->addPreserveText(htmlspecialchars(' Page {PAGE} of {NUMPAGES} '), array('align' => 'center'),array('align'=>'center'));
			}

			$sec2header->addTextBreak();

			return $table;
		}

		function CandidateAttributeQuery(){
			$q_logs = mysql_query("SELECT 
				T1.username, 
				T1.cnd_name, 
				T1.id_battery,
				T1.organization, 
				T1.position, 
				T1.last_name, 
				T1.sex, 
				T1.attribute2, 
				T1.attribute3, 
				T1.attribute4, 
				T1.attribute5, 
				T2.client_logo, 
				T2.client_name, 
				T2.id_client, 
				(SELECT DATE_FORMAT( MAX(time_submit),  '%d/%m/%Y' ) FROM ott_cnd_test T3 WHERE T1.id_cnd = T3.id_cnd)as tgl
					   FROM  `otm_cnd_attribute` AS T1
					   JOIN otm_client AS T2 ON T1.id_client = T2.id_client
					   WHERE T1.id_cnd =  '".$this->idcnd."'") or $array['message']="error mysql : ".mysql_error();
  
			if (mysql_num_rows($q_logs)==0) { $this->array['message']="idcnd ".$this->idcnd." hasil kosong "; }

			$q_log = mysql_fetch_array($q_logs);

			$this->CandidateAttribute = $q_log;

			return $q_log;
		}

		function makeTableRatingHeader($objTable, $text){
			$warnaPurpose = '#2b76c1';

			$color 	= array('color' => 'FFFFFF','align'=>'right','size'=>8);
			$color2	= array('color' => 'FFFFFF','bold'=>true, 'align'=>'center','size'=>12);
			$color3	= array('color' => 'FFFFFF','bold'=>true, 'align'=>'center','size'=>10);

			$bgDark = array(
				'bgColor' => $warnaPurpose, 
				'vMerge' => 'restart', 
				'valign'=>'center'
			);
			$cellRowContinue = array('vMerge' => 'continue');

			$objTable->addRow();

			$objTable->addCell(6100, $bgDark)->addText($text,$color2,array('align'=>'center', 'spaceBefore'=>200));
			$objTable->addCell(2500, array('bgColor'=>$warnaPurpose,'gridSpan'=>5))->addText('RATING',$color3,array('align'=>'center','spaceBefore'=>50,'spaceAfter'=>50));

			$objTable->addRow();

			$objTable->addCell(null, $cellRowContinue);
			$objTable->addCell(500, array('bgColor'=>$warnaPurpose))->addText(htmlspecialchars('1'),$color,'cellGrid');
			$objTable->addCell(500, array('bgColor'=>$warnaPurpose))->addText(htmlspecialchars('2'),$color,'cellGrid');
			$objTable->addCell(500, array('bgColor'=>$warnaPurpose))->addText(htmlspecialchars('3'),$color,'cellGrid');
			$objTable->addCell(500, array('bgColor'=>$warnaPurpose))->addText(htmlspecialchars('4'),$color,'cellGrid');
			$objTable->addCell(500, array('bgColor'=>$warnaPurpose))->addText(htmlspecialchars('5'),$color,'cellGrid');
		}

		function makeTableRatingStar($objTable, $text, $value, $add_row = true){

			$objTable->addRow();

			$celeig = "cellGridBig";
			$textSmallB = array('align'=>'left','size'=>8,'bold'=>true,'spaceAfter' => 0,'spaceBefore' => 0,'color'=>'#000' );

			$objTable->addCell(2500, array('vMerge' => 'restart','valign'=>'center'))
				->addText($text, $textSmallB, array(
					'align'=>'left', 
					'spaceAfter'=>0,
					'spaceBefore'=>0
				));

			$i=1;
			while($i<=5){
				$cell = ($i == 3) ? array('bgColor'=>'#2EB8B8') :  array('bgColor'=>'#FFFFFF');

				if($i==$value){
					$star = ($value < 3) ? "orangestar.gif" : "greenstar.gif";

					$cell3 = $objTable->addCell(340, $cell);

					$textrun = $cell3->createTextRun();
					$textrun->addImage('resources/'.$star, array(
						'positioning'   => 'relative',
						'wrappingStyle' => 'infront',
						'align'=>'center',
						'width' => 15*0.75,
						'height' => 25*0.75,
						'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER,
						'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN
					));

					$textrun->addText("", null, $celeig);
				}else{
					$objTable->addCell(340, $cell)->addText(htmlspecialchars(' '), null, $celeig);
				}

				$i++;
			}
		}

		function IndexDescriptionTemplate($section){
			$isiCel 			= array('size'=>8, 'bold'=>true, 'italic'=>true, 'color'=>'#003399');
			$tab3 				= "blue_tab3.gif";
			$tableStyle3 		= array('align'=>'center', 'borderSize'=> null );
			$tableStyle2 		= array('borderSize'=>1,'borderColor' => $this->warnaPurpose, 'align'=>'center');
			$kcilSize 			= array('bold'=>true, 'color'=>'#FFFFFF','size'=>8);
			$bgWhit 			= array('valign'=>'center');
			$bgDark 			= array('bgColor' => $this->warnaPurpose, 'vMerge' => 'restart', 'valign'=>'center');
			$tableStyle34 		= array('align'=>'center', 'borderSize'=> null , 'cellMarginTop'=>200);

			$tabley = $section->addTable($tableStyle3);

			$tabley->addRow();
			$cell0 = $tabley->addCell(1);
			$tablex = $cell0->addTable($tableStyle3);
			$tablex->addRow();
			$cell3 = $tablex->addCell()->addImage('resources/'.$tab3, array(  'positioning' => 'absolute', 'wrappingStyle' => 'behind', 'width' => 600*0.75, 'height' => 205*0.75, 'align' => 'center'));

		

			$cell0 = $tabley->addCell(9000);
			$tablex = $cell0->addTable($tableStyle34);
			$tablex->addRow();
			$tablex = $tablex->addCell(9000)->addTable($tableStyle2);

			$tablex->addRow();
			$tablex->addCell(500, $bgDark)->addText('1',$kcilSize,'centerGrid');
			$tablex->addCell(1500, $bgWhit)->addText('Weak',$isiCel ,'leftStyle');
			$tablex->addCell(6500, $bgWhit)->addText('Kandidate menunjukkan kecenderungan yang sangat lemah pada trait yang berhubungan dengan kompetensi.',array('size'=>7,'color'=>'#003399'),'isiGrid');

			$tablex->addRow();
			$tablex->addCell(500, $bgDark)->addText('2',$kcilSize,'centerGrid');
			$tablex->addCell(1500, $bgWhit)->addText('Below Average',$isiCel ,'leftStyle');
			$tablex->addCell(6000, $bgWhit)->addText('Kandidate menunjukkan kecenderungan yang lemah pada trait yang berhubungan dengan kompetensi.',array('size'=>7,'color'=>'#003399'),'isiGrid');

			$tablex->addRow();
			$tablex->addCell(500, $bgDark)->addText('3',$kcilSize,'centerGrid');
			$tablex->addCell(1500, $bgWhit)->addText('Average',$isiCel ,'leftStyle');
			$tablex->addCell(6000, $bgWhit)->addText('Kandidate menunjukkan kecenderungan yang memadai pada trait yang berhubungan dengan kompetensi.',array('size'=>7,'color'=>'#003399'),'isiGrid');

			$tablex->addRow();
			$tablex->addCell(500, $bgDark)->addText('4',$kcilSize,'centerGrid');
			$tablex->addCell(1500, $bgWhit)->addText('Above Average',$isiCel ,'leftStyle');
			$tablex->addCell(6000, $bgWhit)->addText('Kandidate menunjukkan kecenderungan yang kuat pada trait yang berhubungan dengan kompetensi.',array('size'=>7,'color'=>'#003399'),'isiGrid');

			$tablex->addRow();
			$tablex->addCell(500, $bgDark)->addText('5',$kcilSize,'centerGrid');
			$tablex->addCell(1500, $bgWhit)->addText('Strong',$isiCel ,'leftStyle');
			$tablex->addCell(6000, $bgWhit)->addText('Kandidate menunjukkan kecenderungan yang sangat kuat pada trait yang berhubungan dengan kompetensi.',array('size'=>7,'color'=>'#003399'),'isiGrid');

			$tabley->addRow();
			$tabley->addCell(1); // Kolom kedua
			$cell3 = $tabley->addCell(8000); // Kolom kedua
			$tablex = $cell3->addTable($tableStyle34);

			$tablex->addRow();
			$tablex->addCell(1);
			$cell3 = $tablex->addCell(9000);
			$tablex = $cell3->addTable($tableStyle2);


			$tablex->addRow();
			$tablex->addCell(500, array('bgColor'=>'#2EB8B8'));
			$tablex->addCell(3750, $bgWhit)->addText('Level Required',$isiCel,'leftStyle');
			$tablex->addCell(500)->addImage('resources/3stars.gif', array(
					'positioning'   => 'relative',
					'wrappingStyle' => 'infront',
					'width' => 20*0.75,
					'height' => 25*0.75,
					'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER,
		      		'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
					'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_CENTER
		    	));
			$tablex->addCell(3750, $bgWhit)->addText('Candidate Level',$isiCel,'leftStyle');
		}

		function Conclusion($arrayAll){
			$section = $this->section;

			$score 	= (int) $this->CandidateAttribute['attribute5'];

			$cellBlue  = array('valign' => 'center','align' => 'center', 'bgColor' => $this->warnaPurpose);

			$kesimpulan = array();

			foreach ($arrayAll as $key ) {

				if($key < 3)
		        {
		            array_push($kesimpulan,$key);
		        }
			}

			$trRek 	= false; 
			$trPer	= false;
			$trTre	= false;

			if( $score > 7 ){
				$recom = "Terbatas";
				$rekom 		= "emptybox.jpg";
				$pertimbang = "emptybox.jpg";
				$tidakRekom = "checkbox.jpg";
				$rgb = "c0504d";
				$trTre		= true;

		    }else if($score <= 4){
				$recom 		= "Luas";
				$rekom 		= "checkbox.jpg";
				$pertimbang = "emptybox.jpg";
				$tidakRekom = "emptybox.jpg";
				$rgb = "9bbb59";
				$trRek		= true;
			}else{
				$recom = "Sedang";
				$rekom 		= "emptybox.jpg";
				$pertimbang = "checkbox.jpg";
				$tidakRekom = "emptybox.jpg";
				$rgb = "ff9933";
				$trPer		= true;
			}

			$table = $section->addTable(array('align'=>'center'));
		    
		    $table->addRow();
		    $table->addCell(9000)->addImage('resources/redbox.gif', array(
                'positioning'   => 'relative',
                'wrappingStyle' => 'behind',
                'width' => 720*0.75,
                'height' => 145*0.75,
                'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER,
                'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
                'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_CENTER
            ));
		    
		    $table = $section->addTable(array('align'=>'center'));
		    
		    $table->addRow();
		    $table->addCell(9000, $cellBlue)->addText(htmlspecialchars('POTENSI'),array('color'=>'#FFFFFF','bold'=>true),'header1');
		    
		    $table->addRow();
		    $table->addCell(9000)->addText(htmlspecialchars("Berdasarkan hasil asesmen yang dilakukan, potensi Sdr. $this->namaCandidate termasuk dalam kategori $recom untuk kriteria-kriteria yang diukur."),array('size'=>9,'name'=>'verdana'),array('align'=>'both','spaceBefore'=>50));
		    $table = $section->addTable(array('align'=>'center'));
		    $table->addRow();
		    $table->addCell(100)->addImage('resources/'.$rekom,array('width'=>30*0.75));
		    $table->addCell(2500)->addText(htmlspecialchars("Luas"),array('color'=>$this->warnaPurpose,'bold'=>$trRek, 'italic'=>true, 'size'=>9),array('spaceBefore'=>100));
		    $table->addCell(100)->addImage('resources/'.$pertimbang,array('width'=>30*0.75));
		    $table->addCell(2500)->addText(htmlspecialchars("Sedang"),array('color'=>$this->warnaPurpose, 'bold'=>$trPer,'italic'=>true, 'size'=>9),array('spaceBefore'=>100));
		    $table->addCell(100)->addImage('resources/'.$tidakRekom,array('width'=>30*0.75));
		    $table->addCell(3500)->addText(htmlspecialchars("Terbatas"),array('color'=>$this->warnaPurpose, 'bold'=>$trTre, 'italic'=>true, 'size'=>9),array('spaceBefore'=>100));
		}

		function Naration(){
			$section = $this->section;

			$fontKecil = array('size'=>10);
			$normaPer   = isset($_GET['norm_personality']) ? $_GET['norm_personality'] : ''; //indgenpop
			$cellBlue  = array('valign' => 'center','align' => 'center', 'bgColor' => $this->warnaPurpose);

			# Hitungan Narasi
			$boleh_arr = array('fQ1', 'fI', 'fM', 'fQ3' ,'fG', 'fC','fO','fQ4');

			$a_logsT = mysql_query("SELECT DATE_FORMAT( B.time_submit,  '%d/%m/%Y' ) AS tgl, A.id_test, A.set_dimension, A.pts_score
			                    FROM  `ott_cnd_score` AS A
			                    JOIN  `ott_cnd_test` AS B ON A.id_cnd = B.id_cnd AND A.id_test = B.id_test
			                    WHERE A.id_cnd =  '".$this->idcnd."' 
			                    ORDER BY id_test") or $this->array['message']="error mysql : ".mysql_error();

			if (mysql_num_rows($a_logsT)==0) { 
				$this->array['message']="empty result pada hitungan narasi ott_cnd_score join ott_cnd_test"; 
			}

			/**
			NILAI NARASI
			**/
			$result_narasi = array();

			while($a_log = mysql_fetch_array($a_logsT)){

				if($a_log['id_test']=='6' && in_array($a_log['set_dimension'],$boleh_arr) || $a_log['id_test']=='7' && in_array($a_log['set_dimension'],$boleh_arr)){

					$v_ptsscore = $a_log['pts_score'];
					$aspect = $a_log['set_dimension'];
					$v_logs     = mysql_query("SELECT A.final_score AS score, B.narrative AS narasi, B.category
					                          FROM  otm_test_norm_key AS A
					                          JOIN otm_test_narrative AS B ON A.final_score = B.final_score AND A.id_test = B.id_test AND A.set_dimension = B.aspect
					                          JOIN otm_test_norm AS C ON A.id_norm = C.id_norm
					                          WHERE A.id_test =  '".$a_log['id_test']."'
					                          AND A.set_dimension =  '".$aspect."'
					                          AND A.raw_score =  '".$v_ptsscore."'
					                          AND B.test_language = 'indonesia'
					                          AND C.norm_code='".$normaPer."'") or $this->array['message']="error mysql : ".mysql_error();

					if (mysql_num_rows($v_logs)==0) { $array['message']="empty result pada hitungan narasi ott_cnd_score join ott_cnd_test"; }

					while($row = mysql_fetch_array($v_logs)) {
					  	$result_narasi[$aspect] = str_replace('NAMAKANDIDAT', $this->namaCandidate, $row['narasi']);
					}
				}
			}

			$table = $section->addTable(array('cellMarginTop'=>50,'cellMarginBottom'=>40));
			$table->addRow();
			$table->addCell(11000, $cellBlue)->addText(htmlspecialchars('INFORMASI TAMBAHAN'),array('color'=>'#FFFFFF','bold'=>true),'centerGrid');
			$table->addRow();
			$table->addCell(9000, $cellBlue)->addText(htmlspecialchars('(Sumber : Tes Kepribadian)'),array('color'=>'#FFFFFF','size'=>8),'centerGrid');

			$section->addTextBreak(1);

			$table = $section->addTable();
			$table->addRow();
			$table->addCell(11000)->addText('CARA BERPIKIR', array(
				'bold'=>true,
				'underline'=>\PhpOffice\PhpWord\Style\Font::UNDERLINE_SINGLE, 
				'size'=>9
			),'centerGrid');

			$table->addRow();
			$table->addCell(9000)->addText(htmlspecialchars(trim($result_narasi['fQ1'])),$fontKecil,'pStyle');

			$table->addRow();
			$table->addCell(9000)->addText(htmlspecialchars(trim($result_narasi['fI'])),$fontKecil,'pStyle');

			$table->addRow();
			$table->addCell(9000)->addText(htmlspecialchars(trim($result_narasi['fM'])),$fontKecil,'pStyle');

			$table->addRow();
			$table->addCell(9000)->addText(htmlspecialchars(trim($result_narasi['fQ3'])),$fontKecil,'pStyle');

			$table->addRow();
			$table->addCell(9000)->addText(htmlspecialchars(trim($result_narasi['fG'])),$fontKecil,'pStyle');
		
			$section->addPageBreak();

			$table = $section->addTable();
			$table->addRow();
			$table->addCell(11000)->addText('PENANGANAN TEKANAN KERJA',array('bold'=>true,'underline'=>\PhpOffice\PhpWord\Style\Font::UNDERLINE_SINGLE, 'size'=>9, 'spaceAfter'=>100),'centerGrid');

			$table->addRow();
			$table->addCell(9000)->addText(htmlspecialchars(trim($result_narasi['fC'])),$fontKecil,'pStyle');

			$table->addRow();
			$table->addCell(9000)->addText(htmlspecialchars(trim($result_narasi['fO'])),$fontKecil,'pStyle');

			$table->addRow();
			$table->addCell(9000)->addText(htmlspecialchars(trim($result_narasi['fQ4'])),$fontKecil,'pStyle');
		}

		function IntroducePelniBox(){
			$fontWhite 	= array('color' => '#FFFFFF','align'=>'left','size'=>9,'spaceAfter' => 0,'spaceBefore' => 30, 'bold'=>true);
			$fontBlack 	= array('color' => '#00246B','align'=>'left','size'=>9,'spaceAfter' => 0,'spaceBefore' => 30, 'bold'=>true);
			$bgBlue  	= array('valign' => 'center','align' => 'left', 'bgColor' => $this->warnaPurpose);

			$level 	= $this->CandidateAttribute['attribute3'];
			$posisi = $this->CandidateAttribute['position'];
			$sexx 	= ($this->CandidateAttribute['sex'] == "Male") ? "Laki-Laki" : "Perempuan";
			$tgls 	= $this->CandidateAttribute['tgl'];

			$table = $this->section->addTable('Colspan Rowspan');

			$table->addRow();
			$table->addCell(5000, array('gridSpan'=>2,'align'=>'center','bgColor'=>$this->warnaPurpose))->addText($this->namaCandidate,array('color'=>'#FFFFFF','size'=>9, 'bold'=>true,'align'=>'center','spaceBefore' => 30),'centerGrid');
			$table->addRow();
			$table->addCell(2000, $bgBlue)->addText('Level',$fontWhite,'listGrid');
			$table->addCell(3000)->addText(htmlspecialchars($level), $fontBlack,'listGrid');
			$table->addRow();
			$table->addCell(2000, $bgBlue)->addText('Posisi',$fontWhite,'listGrid');
			$table->addCell(3000)->addText(htmlspecialchars($posisi), $fontBlack,'listGrid');
			$table->addRow();
			$table->addCell(2000, $bgBlue)->addText('Jenis Kelamin',$fontWhite,'listGrid');
			$table->addCell(3000)->addText($sexx, $fontBlack,'listGrid');
			$table->addRow();
			$table->addCell(2000, $bgBlue)->addText('Tanggal Asesmen',$fontWhite,'listGrid');
			$table->addCell(3000)->addText($tgls, $fontBlack,'listGrid');
		}

		function FieldCapabilityTestScore(){
			$tab3 	= "blue_tab3.gif";
			$color2	= array('color' => 'FFFFFF','bold'=>true, 'align'=>'center','size'=>12);

			$level 	= $this->CandidateAttribute['attribute3'];
			$posisi = $this->CandidateAttribute['position'];
			$score 	= $this->CandidateAttribute['attribute4'];

			$full_text = sprintf('%s - %s', $level, $posisi);

			$section = $this->section;

			$bgDark = array(
				'bgColor' => $this->warnaPurpose, 
				'vMerge' => 'restart', 
				'valign'=>'center'
			);

			/* begin - add section tes kemampuan bidang */
		    $table = $section->addTable(array('align'=>'center', 'borderColor' => '#000'));
		    $table->addRow();
		    $table->addCell(9000)->addImage('resources/'.$tab3, array(
		                'positioning'   => 'relative',
		                'wrappingStyle' => 'behind',
		                'width' => 720*0.75,
		                'height' => 145*0.65,
		                'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER,
		                'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
		                'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_CENTER
		            ));

		    $default_cell = array('bold' => true, 'align'=>'left','size'=>12,'spaceAfter' => 0,'color'=>'#000');
		    
		    $table = $section->addTable(array('align'=>'center','borderColor'=>$this->warnaPurpose, 'borderSize' => 1));

		    $table->addRow();
		    $table->addCell(6000, $bgDark)
		      ->addText('Tes Kemampuan Bidang', $color2, array('align'=>'center', 'spaceBefore'=>200));
		    $table->addCell(3000, $bgDark)
		      ->addText('SKOR', $color2, array('align'=>'center', 'spaceBefore'=>200));

		    $table->addRow();
		    $table->addCell(6000, array('valign'=>'center'))
		      ->addText(htmlspecialchars($full_text), $default_cell,array('align'=>'left', 'spaceAfter'=>0,'spaceBefore'=>0));

		    $table->addCell(3000, array('valign'=>'center'))
		      ->addText($score, $default_cell,array('align'=>'center', 'spaceAfter'=>0,'spaceBefore'=>0));

		    $section->addTextBreak(2);
		    /* end - add section tes kemampuan bidang */
		}

		function FooterIntroduce(){
			$tab1 = "blue_tab1.gif"; 
			$style12 = array('borderSize' => null,'align'=>'center');
	
			$footer = $this->section->addFooter();
			$table = $footer->addTable($style12);
			$table->addRow();

			$table->addCell(1)->addImage('resources/'.$tab1, array(  'positioning'   => 'relative','wrappingStyle' => 'behind', 'width' => 610*0.75, 'height' => 55*0.75, 'align' => 'left'));
			$table->addCell(1250,array('cellMargin'=>30))->addImage('resources/logo_smart.gif', array(  'positioning'   => 'relative','wrappingStyle' => 'behind', 'height' => 45*0.75, 'align' => 'left', 'valign' => 'bottom'));
			$table->addCell(7750)->addText('Laporan asesmen ini bersifat sangat rahasia dan berisi informasi yang sensitif. Informasi dalam laporan ini wajib disimpan dengan aman. Tidak dibuka dan diberikan kepada orang-orang yang tidak berkepentingan dan tidak diperkenankan tanpa seijin dari pihak kami.',array('size'=>7,'bold'=>true, 'italic' => true),'dStyle');
		}
	}
?>